const tasks = [
  {
    _id: "5d2ca9e2e03d40b326596aa7",
    completed: true,
    body:
      "Occaecat non ea quis occaecat ad culpa amet deserunt incididunt elit fugiat pariatur. Exercitation commodo culpa in veniam proident laboris in. Excepteur cupidatat eiusmod dolor consectetur exercitation nulla aliqua veniam fugiat irure mollit. Eu dolor dolor excepteur pariatur aute do do ut pariatur consequat reprehenderit deserunt.\r\n",
    title: "Eu ea incididunt sunt consectetur fugiat non.",
  },
  {
    _id: "5d2ca9e29c8a94095c1288e0",
    completed: false,
    body:
      "Aliquip cupidatat ex adipisicing veniam do tempor. Lorem nulla adipisicing et esse cupidatat qui deserunt in fugiat duis est qui. Est adipisicing ipsum qui cupidatat exercitation. Cupidatat aliqua deserunt id deserunt excepteur nostrud culpa eu voluptate excepteur. Cillum officia proident anim aliquip. Dolore veniam qui reprehenderit voluptate non id anim.\r\n",
    title:
      "Deserunt laborum id consectetur pariatur veniam occaecat occaecat tempor voluptate pariatur nulla reprehenderit ipsum.",
  },
  // {
  //   _id: "5d2ca9e2e03d40b3232496aa7",
  //   completed: true,
  //   body:
  //     "Occaecat non ea quis occaecat ad culpa amet deserunt incididunt elit fugiat pariatur. Exercitation commodo culpa in veniam proident laboris in. Excepteur cupidatat eiusmod dolor consectetur exercitation nulla aliqua veniam fugiat irure mollit. Eu dolor dolor excepteur pariatur aute do do ut pariatur consequat reprehenderit deserunt.\r\n",
  //   title: "Eu ea incididunt sunt consectetur fugiat non.",
  // },
  // {
  //   _id: "5d2ca9e29c8a94095564788e0",
  //   completed: false,
  //   body:
  //     "Aliquip cupidatat ex adipisicing veniam do tempor. Lorem nulla adipisicing et esse cupidatat qui deserunt in fugiat duis est qui. Est adipisicing ipsum qui cupidatat exercitation. Cupidatat aliqua deserunt id deserunt excepteur nostrud culpa eu voluptate excepteur. Cillum officia proident anim aliquip. Dolore veniam qui reprehenderit voluptate non id anim.\r\n",
  //   title:
  //     "Deserunt laborum id consectetur pariatur veniam occaecat occaecat tempor voluptate pariatur nulla reprehenderit ipsum.",
  // },
];

const abs = [{
  name: 'asas',
  age: 20
},
  {
    name: 'bbb',
    age: 20
  }
]

console.log(asw(abs))


function asw(el) {
  const newArr = [...el].filter((task)=>{
  let res = task.age
    console.log(res)

  })
  console.log(newArr)

}

(function (arrOfTasks) {
  // * Самовызывающаяся функция испольуется для того, что бы переменные не вышли в глобальную область видимости

  const objOfTask = arrOfTasks.reduce((acc, task) => {
    acc[task._id] = task; // * acc[task._id] - как ключ, task(объект) - как значение. acc тут объект
    return acc;
  }, {});

  const themes = {
    default: {
      "--base-text-color": "#212529",
      "--header-bg": "#007bff",
      "--header-text-color": "#fff",
      "--default-btn-bg": "#007bff",
      "--default-btn-text-color": "#fff",
      "--default-btn-hover-bg": "#0069d9",
      "--default-btn-border-color": "#0069d9",
      "--danger-btn-bg": "#dc3545",
      "--danger-btn-text-color": "#fff",
      "--danger-btn-hover-bg": "#bd2130",
      "--danger-btn-border-color": "#dc3545",
      "--input-border-color": "#ced4da",
      "--input-bg-color": "#fff",
      "--input-text-color": "#495057",
      "--input-focus-bg-color": "#fff",
      "--input-focus-text-color": "#495057",
      "--input-focus-border-color": "#80bdff",
      "--input-focus-box-shadow": "0 0 0 0.2rem rgba(0, 123, 255, 0.25)",
    },
    dark: {
      "--base-text-color": "#212529",
      "--header-bg": "#343a40",
      "--header-text-color": "#fff",
      "--default-btn-bg": "#58616b",
      "--default-btn-text-color": "#fff",
      "--default-btn-hover-bg": "#292d31",
      "--default-btn-border-color": "#343a40",
      "--default-btn-focus-box-shadow":
        "0 0 0 0.2rem rgba(141, 143, 146, 0.25)",
      "--danger-btn-bg": "#b52d3a",
      "--danger-btn-text-color": "#fff",
      "--danger-btn-hover-bg": "#88222c",
      "--danger-btn-border-color": "#88222c",
      "--input-border-color": "#ced4da",
      "--input-bg-color": "#fff",
      "--input-text-color": "#495057",
      "--input-focus-bg-color": "#fff",
      "--input-focus-text-color": "#495057",
      "--input-focus-border-color": "#78818a",
      "--input-focus-box-shadow": "0 0 0 0.2rem rgba(141, 143, 146, 0.25)",
    },
    light: {
      "--base-text-color": "#212529",
      "--header-bg": "#fff",
      "--header-text-color": "#212529",
      "--default-btn-bg": "#fff",
      "--default-btn-text-color": "#212529",
      "--default-btn-hover-bg": "#e8e7e7",
      "--default-btn-border-color": "#343a40",
      "--default-btn-focus-box-shadow":
        "0 0 0 0.2rem rgba(141, 143, 146, 0.25)",
      "--danger-btn-bg": "#f1b5bb",
      "--danger-btn-text-color": "#212529",
      "--danger-btn-hover-bg": "#ef808a",
      "--danger-btn-border-color": "#e2818a",
      "--input-border-color": "#ced4da",
      "--input-bg-color": "#fff",
      "--input-text-color": "#495057",
      "--input-focus-bg-color": "#fff",
      "--input-focus-text-color": "#495057",
      "--input-focus-border-color": "#78818a",
      "--input-focus-box-shadow": "0 0 0 0.2rem rgba(141, 143, 146, 0.25)",
    },
  };

  let lastSelectedTheme = localStorage.getItem("app_theme") || "default";

  //? Elements UI

  const listContainer = document.querySelector(
    ".tasks-list-section .list-group"
  );
  const btnCheckCompleted = document.querySelector(".btn-completed");
  const btnCheckUnfinished = document.querySelector(".btn-unfinished");
  const btnCheckAllTask = document.querySelector(".btn-all");
  const themeSelect = document.getElementById("themeSelect");

  const form = document.forms["addTask"]; // * получаем форму по атрибуту name
  const inputTitle = form.elements["title"];
  const inputBody = form.elements["body"]; // * получаем input в форме по атрибуту name

  //? Events

  emptyTasks(arrOfTasks);
  setTheme(lastSelectedTheme);
  renderAllTask(objOfTask);
  form.addEventListener("submit", onFormSubmitHandler);
  listContainer.addEventListener("click", onDeleteHandler);
  listContainer.addEventListener("click", onSuccessHandler);
  btnCheckCompleted.addEventListener("click", onCompletedTask);
  btnCheckUnfinished.addEventListener("click", onUnfinishedTask);
  btnCheckAllTask.addEventListener("click", onAllTask);
  themeSelect.addEventListener("change", onThemeSelectHandler);

  function renderAllTask(taskList) {
    if (!taskList) {
      return alert("Передайти список задач!");
    }
    const fragment = document.createDocumentFragment();
    //*  Object.value принимает объект и возвращает его значение в виде массива
    Object.values(taskList).forEach((task) => {
      const li = listItemTemplate(task);
      fragment.appendChild(li);
    });
    listContainer.appendChild(fragment);
  }

  function listItemTemplate({ _id, title, body, completed } = {}) {
    const li = document.createElement("li");

    if (completed) {
      li.classList.add("border-success", "bg-light", "success");
    }

    li.classList.add(
      "list-group-item",
      "d-flex",
      "border",
      "align-items-center",
      "flex-wrap",
      "mb-4"
    );
    li.setAttribute("data-task-id", _id);

    const span = document.createElement("span");
    span.textContent = title;
    span.style.fontWeight = "bold";

    const deletBtn = document.createElement("button");
    deletBtn.textContent = "Delete";
    deletBtn.classList.add("btn", "btn-danger", "ml-auto", "delete-btn");

    const article = document.createElement("p");
    article.textContent = body;
    article.classList.add("mt-2", "w-100");

    const successBtn = document.createElement("button");
    successBtn.textContent = "Done";
    successBtn.classList.add("btn", "ml-auto", "btn-success", "success-btn");

    li.appendChild(span);
    li.appendChild(successBtn);
    li.appendChild(article);
    li.appendChild(deletBtn);

    return li;
  }

  function onFormSubmitHandler(e) {
    e.preventDefault();
    const titleValue = inputTitle.value;
    const bodyValue = inputBody.value;

    if (!titleValue || !bodyValue) {
      alert("Заполните поля");
      return;
    }

    const task = createNewTask(titleValue, bodyValue);
    const listItem = listItemTemplate(task);
    listContainer.insertAdjacentElement("afterbegin", listItem);
    form.reset(); //* сбросить состояние формы
  }

  function createNewTask(title, body) {
    hideEmptyNotification();
    const newTask = {
      title: title,
      body: body,
      completed: false,
      _id: `task-${Math.random()}`,
    };

    objOfTask[newTask._id] = newTask;
    localStorage.setItem("app_task", JSON.stringify(newTask));

    return { ...newTask }; // * копия объекта
  }

  function deleteTask(id) {
    const { title } = objOfTask[id];
    const isConfirm = confirm(
      `Вы действительно хотите удалить задачу ${title}?`
    );

    if (!isConfirm) return isConfirm;

    delete objOfTask[id];
    return isConfirm;
  }

  function deleteTaskFromHtml(confirmed, el) {
    if (!confirmed) return;
    el.remove();
    emptyTasks();
  }

  function onDeleteHandler({ target }) {
    if (target.classList.contains("delete-btn")) {
      const parent = target.closest("[data-task-id]"); // * ищем ближайшего родителя
      const id = parent.dataset.taskId;
      const confirmed = deleteTask(id);
      deleteTaskFromHtml(confirmed, parent);
    }
  }

  function successTask(id, el) {
    const flag = objOfTask[id];
    flag.completed = true;
    el.classList.add("border-success", "bg-light", "success");
  }

  function onSuccessHandler({ target }) {
    if (target.classList.contains("success-btn")) {
      const parent = target.closest("[data-task-id]");
      const id = parent.dataset.taskId;
      successTask(id, parent);
    }
  }

  function onCompletedTask() {
    clearTask();

    const completed = Object.values(objOfTask).filter((task) => {
      if (task.completed) {
        emptyTasks(task);
        const listItem = listItemTemplate(task);
        listContainer.insertAdjacentElement("afterbegin", listItem);
        console.log(task);
      }
    });
    return completed;
  }

  function onUnfinishedTask() {
    clearTask();
    emptyTasks();
    const unfinished = Object.values(objOfTask).filter((task) => {
      if (!task.completed) {
        const listItem = listItemTemplate(task);
        listContainer.insertAdjacentElement("afterbegin", listItem);
      }
    });
    return unfinished;
  }

  function onAllTask() {
    clearTask();
    emptyTasks();
    const allTasks = Object.values(objOfTask).sort(
      (prev, next) => prev.completed - next.completed
    );
    renderAllTask(allTasks);
  }

  function emptyTextTask() {
    const empty = document.createElement("p");
    empty.textContent = "Нет задач";
    empty.classList.add("no-task", "text-center", "font-weight-bold");
    return empty;
  }

  function emptyTasks() {
    if (!Object.keys(objOfTask).length) {
      const empty = emptyTextTask();
      listContainer.appendChild(empty);
      return empty;
    }
  }

  function hideEmptyNotification() {
    const notification = document.querySelector(".no-task");
    if (notification) {
      notification.remove();
    }
  }

  function clearTask() {
    listContainer.innerHTML = "";
  }

  function onThemeSelectHandler() {
    const selectedTheme = themeSelect.value;
    const isConfirm = confirm(`Изменить тему: ${selectedTheme}`);
    if (!isConfirm) {
      themeSelect.value = lastSelectedTheme;
      return;
    }
    setTheme(selectedTheme);
    lastSelectedTheme = selectedTheme;
    localStorage.setItem("app_theme", selectedTheme);
  }

  function setTheme(name) {
    const selectedThemeObj = themes[name]; // * получаем из объекта нужный элемент по ключу
    Object.entries(selectedThemeObj).forEach(([key, value]) => {
      document.documentElement.style.setProperty(key, value);
    });
  }
})(tasks);
