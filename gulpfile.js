const gulp = require("gulp");
const stylus = require("gulp-stylus");
const plumber = require("gulp-plumber");
const notify = require("gulp-notify");
const autoprefixer = require("gulp-autoprefixer");
const cleanCSS = require("gulp-clean-css");
const rigger = require("gulp-rigger");
const del = require("del");
const imagemin = require("gulp-imagemin");
const imageminWebp = require("imagemin-webp");
const svgSprite = require("gulp-svg-sprite");
const svgo = require("gulp-svgo");
const cache = require("gulp-cache");
const browserSync = require("browser-sync").create();
const minify = require("gulp-minify");
const extReplace = require("gulp-ext-replace");
const purgecss = require("gulp-purgecss");
const cheerio = require("gulp-cheerio");
const cleanSvg = require("gulp-cheerio-clean-svg");
const shorthand = require("gulp-shorthand");
const pxtorem = require("gulp-pxtorem");

const cssFiles = [
  "src/style/style.styl",
  "src/style/build.styl",
  // 'src/style/chunk/*.styl' //* в продакшн
];
const libCssFiles = "src/style/dist/*";
const libJsFiles = "src/js/dist/*";
const htmlFiles = "src/*.html";
const jsFiles = "src/js/main.js";
const imageFiles = "src/img/**/*.*";
const fontsFiles = "src/fonts/**/*.*";
const svgFiles = "src/svg/**/*.*";
const optimizeSvg = "src/svg/*-icon.svg";

gulp.task("clear", () => cache.clearAll());

function style() {
  return gulp
    .src(cssFiles)
    .pipe(
      plumber({
        errorHandler: notify.onError((err) => ({
          title: "ERROR Stylus Сompilation",
          message: err.message,
        })),
      })
    )
    .pipe(stylus())
    .pipe(pxtorem())
    .pipe(autoprefixer({ cascade: false }))
    .pipe(shorthand())
    .pipe(cleanCSS({ debug: true, compatibility: "*" }))
    .pipe(
      purgecss({
        content: ["src/*.html"],
        whitelistPatterns: [/active/, /show/, /locked/],
      })
    )
    .pipe(gulp.dest("build/css/"))
    .pipe(browserSync.stream());
}

function libCss() {
  return gulp
    .src(libCssFiles)
    .pipe(gulp.dest("build/css/"))
    .pipe(browserSync.stream());
}

function html() {
  return gulp
    .src(htmlFiles)
    .pipe(rigger())
    .pipe(gulp.dest("build/"))
    .pipe(browserSync.stream());
}

function libJs() {
  return gulp
    .src(libJsFiles)
    .pipe(gulp.dest("build/js/"))
    .pipe(browserSync.stream());
}

function js() {
  return gulp
    .src(jsFiles)
    .pipe(
      plumber({
        errorHandler: notify.onError((err) => ({
          title: "ERROR JS",
          message: err.message,
        })),
      })
    )
    .pipe(minify())
    .pipe(gulp.dest("build/js/"))
    .pipe(browserSync.stream());
}

function imageWebp() {
  return gulp
    .src(imageFiles)
    .pipe(
      imagemin([
        imageminWebp({
          quality: 90,
        }),
      ])
    )
    .pipe(extReplace(".webp"))
    .pipe(gulp.dest("build/img/"))
    .pipe(browserSync.stream());
}

function image() {
  return gulp
    .src(imageFiles)
    .pipe(
      imagemin([
        imagemin.gifsicle({
          interlaced: true,
        }),
        imagemin.mozjpeg({
          quality: 75,
          progressive: true,
        }),
        imagemin.optipng({
          optimizationLevel: 5,
        }),
        imagemin.svgo({
          plugins: [
            {
              removeViewBox: true,
            },
            {
              cleanupIDs: false,
            },
          ],
        }),
      ])
    )
    .pipe(gulp.dest("build/img/"))
    .pipe(browserSync.stream());
}

function fonts() {
  return gulp.src(fontsFiles).pipe(gulp.dest("build/fonts/"));
}

function svgOptimize() {
  return gulp
    .src(optimizeSvg)
    .pipe(
      cheerio(
        cleanSvg({
          attributes: ["style", "fill*"],
          parserOptions: {
            xmlMode: true,
          },
        })
      )
    )
    .pipe(browserSync.stream());
}

function svg() {
  return gulp
    .src(svgFiles)
    .pipe(svgo())
    .pipe(
      svgSprite({
        mode: {
          stack: {
            sprite: "../sprite.svg",
          },
        },
      })
    )
    .pipe(gulp.dest("build/img/"))
    .pipe(browserSync.stream());
}

function clean() {
  return del(["build/*"]);
}

function watch() {
  browserSync.init({
    server: {
      baseDir: "./build",
    },
    tunnel: false,
    notify: false,
    host: "localhost",
    port: 9000,
    logPrefix: "MyBuild",
  });

  gulp.watch("src/style/**/*.styl", style);
  gulp.watch("src/style/dist/*.css", libCss);
  gulp.watch("src/js/**/*.js", js);
  gulp.watch("src/style/dist/*.js", libJs);
  gulp.watch("src/**/*.html", html).on("change", browserSync.reload);
  gulp.watch("src/img/**/*.*", image).on("change", browserSync.reload);
  gulp.watch("src/img/**/*.*", imageWebp).on("change", browserSync.reload);
  gulp.watch("src/svg/**/*.*", svgOptimize).on("change", browserSync.reload);
  gulp.watch("src/svg/**/*.*", svg).on("change", browserSync.reload);
}

gulp.task("libCss", libCss);
gulp.task("style", style);
gulp.task("libJs", libJs);
gulp.task("js", js);
gulp.task("html", html);
gulp.task("image", image);
gulp.task("imageWebp", imageWebp);
gulp.task("fonts", fonts);
gulp.task("svg", svg);
gulp.task("svgOptimize", svgOptimize);
gulp.task("watch", watch);
gulp.task(
  "build",
  gulp.series(
    clean,
    gulp.parallel(style, libCss, libJs, js, html, fonts, svgOptimize, svg)
  )
); // * imageWebp, image,

gulp.task("default", gulp.series("build", "clear", "watch"));
